ARG DOTNET_CORE_VERSION
ARG ASPNET_RUNTIME_VERSION

FROM mcr.microsoft.com/dotnet/sdk:$DOTNET_CORE_VERSION AS build-env

WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./

RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:$ASPNET_RUNTIME_VERSION
WORKDIR /app

COPY --from=build-env /app/out .

ENTRYPOINT ["dotnet", "Gx.Blueprint.dll"]
