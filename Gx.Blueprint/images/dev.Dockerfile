ARG DOTNET_CORE_VERSION

FROM mcr.microsoft.com/dotnet/sdk:$DOTNET_CORE_VERSION

WORKDIR /app

ENTRYPOINT [ "dotnet", "watch", "run" ]
