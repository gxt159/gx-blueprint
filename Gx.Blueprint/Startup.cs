namespace Gx.Blueprint
{
    using System;
    using System.IO;
    using System.Reflection;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;

    /// <summary>
    /// Class, responsible for configuring the app, tuning of services and request pipes.
    /// </summary>
    public class Startup
    {
        private const string PublicCorsPolicyName = "PublicPolicy";

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Gathered configuration object.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Register services, which are necessary for the app.<br/>
        /// Methods have signature "Add[ServiceName]".
        /// </summary>
        /// <param name="services">Collection of services to add.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddCors(options =>
            {
                options.AddPolicy(PublicCorsPolicyName, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            var appVersion = Configuration["AppVersion"];
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(appVersion, new OpenApiInfo
                {
                    Version = appVersion,
                    Title = "Blueprint service API",
                });

                // Set the comments path for the Swagger JSON and UI.
                // Source: https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio#xml-comments
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Configures, how the app will proceed requests.
        /// </summary>
        /// <param name="app">Object, responsible for setting components of the app.<br/>
        /// Methods have signature "Use[ComponentName]".</param>
        /// <param name="env">Object, responsible for the application environment (get and interact with it).</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/error/development");
            }
            else
            {
                app.UseExceptionHandler("/error/production");
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseCors(PublicCorsPolicyName);

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{Configuration["AppVersion"]}/swagger.json", "Gx.Blueprint API");
                c.RoutePrefix = "blueprint";
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
