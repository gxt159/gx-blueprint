namespace Gx.Blueprint
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;

    // ReSharper disable once ClassNeverInstantiated.Global

    /// <summary>
    /// Class responsible for providing an entry point of the app.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point of the app.
        /// </summary>
        /// <param name="args">CLI arguments.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Creates host builder and configure it.
        /// </summary>
        /// <param name="args">CLI arguments.</param>
        /// <returns>Configured host builder.</returns>
        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    config.AddJsonFile("appsettings.json")
                        .AddJsonFile("secrets/appsettings.private.json", true);
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}
