# Gx.Blueprint

Blueprint project for C# microservices

# Setup
1. Clone this repo
2. Replace all occurrences of
* gx-blueprint
* Gx.Blueprint
* GX_BLUEPRINT

with your desire app name

# Launch
This blueprint repository comes with base configuration for launch in docker container.
### Pros
1. Requires only docker installed
2. Can be easily expanded and connected with other services, launched locally
### Cons
1. Slower
2. Needs more resources
3. Needs docker installation (you might face some issues with Windows installation)

Run this command in project root directory (look for .sln file location)
```
docker-compose -f ./Gx.Blueprint/docker-compose.yml up
```

In your CLI you should see something like:
```
...
gx-blueprint_1  | watch : Polling file watcher is enabled
gx-blueprint_1  | watch : Started
gx-blueprint_1  | info: Microsoft.Hosting.Lifetime[0]
gx-blueprint_1  |       Now listening on: http://[::]:8080
gx-blueprint_1  | info: Microsoft.Hosting.Lifetime[0]
gx-blueprint_1  |       Application started. Press Ctrl+C to shut down.
gx-blueprint_1  | info: Microsoft.Hosting.Lifetime[0]
gx-blueprint_1  |       Hosting environment: Production
gx-blueprint_1  | info: Microsoft.Hosting.Lifetime[0]
gx-blueprint_1  |       Content root path: /app
```

Now the service is available on http://0.0.0.0:8080

### Note
App launch with `dotnet watch run` by default. This command provides an opportunity to hot rebuild your application when your source code changes.

If you want to use simple `dotnet run`, then you should edit `./Docker/dev.Dockerfile` as you wish.
