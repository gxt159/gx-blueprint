namespace Gx.Blueprint.Tests.Functional
{
    using System;
    using System.Net;
    using System.Net.Http;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.TestHost;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Xunit;

    /// <summary>
    /// Test class of the app health check methods.
    /// </summary>
    public class HealthControllerTest
    {
        /// <summary>
        /// Method GET /health/ping should return Ok result every time.
        /// </summary>
        [Fact]
        public async void Returns_Ok_OnPing()
        {
            // Arrange.
            using var host = await new HostBuilder()
                .ConfigureAppConfiguration((context, config) =>
                {
                    config.AddJsonFile("appsettings.json");
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseTestServer()
                        .UseStartup<Startup>();
                })
                .StartAsync();
            var client = host.GetTestClient();

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("http://localhost/health/ping"),
            };

            // Act.
            var result = await client.SendAsync(request);

            // Assert.
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }
    }
}
